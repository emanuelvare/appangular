import { Component, ɵConsole } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',

})
export class AppComponent {
  letras = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "Ñ", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "Y", "Z"];
  palabra = "MURCIELAGO";
  palabraOculta = "";
  intentos = 0;
  longpalabra = 0;
  gano = false;
  perdio = false;

  constructor() {

    /* this.palabraOculta ="-".repeat(this.palabra.length);*/
    for (let i = 0; i < this.palabra.length; i++) {

      this.palabraOculta = this.palabraOculta + "-";
      this.longpalabra = this.palabraOculta.length;


    }
    console.log(this.palabraOculta);

  }



  comprobar(letra) {

    this.existeletra(letra);
    let palabraOcultaArr = this.palabraOculta.split('');
    for (let i = 0; i < this.palabra.length; i++) {
      if (this.palabra[i] === letra) {
        palabraOcultaArr[i] = letra;
      }
    }
    this.palabraOculta = palabraOcultaArr.join('');
    this.verificaGana();
  }
  existeletra(letra) {
    if (this.palabra.indexOf(letra) >= 0) {

    }
    else {
      if (this.intentos <= 9) {
        this.intentos++;
       // let intentos2 = 9 - this.intentos;
        //alert("te falta " + intentos2 + " intentos");
      }

    }
   
  }
  verificaGana(){
    if (this.palabraOculta === this.palabra) {
      //alert("usuario gano");
      this.gano = true;
    }
    if (this.intentos >= 9) {
      //alert("usuario perdio");
      this.perdio = true;
    }
  }
}
